# QNode Client

The QNode Client is a program that users or system administrators install on a computer or server to help process Quinn messages. This takes the load off of our servers.

## Usage

Run `npm start`

## Support

For code support, contact developers@solusproject.org

For questions or concerns about the Solus Project, contact support@solusproject.org

## Contributing

We are always open to contributions! If you feel like you can add something, feel free to make a pull request and we'll have our team of caffeinated developers review it! We love seeing our community come together to make the Solus Project what it is.

## Authors and Acknowledgements

Head Developer: William Kenzie

## License

All of our projects use the MIT License, which means you can remix or redistribute what we make however you see fit, as long as you acknowledge that you used our code.

Content like images are always licensed under the CC-BY-SHA license unless stated otherwise.

