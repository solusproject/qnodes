const wsReq = require("ws");
const ws = new wsReq('ws://localhost:3131');

ws.on('open', () => {

    console.log("[!] Connected to the QNODE server!");

    ws.send(JSON.stringify({
        type: "init",
        data: {}
    }));

    /* Use an API request to the QNode server on port 8080 to read from AI
    ex localhost:5005/model/parse 
    ex json:
    {
    "text": "Hello, I am Rasa!",
    "message_id": "b2831e73-1407-4ba0-a861-0f30a42a2a5a"
    }
    */
});