# QNode Server

The QNode Server Software is a "trusted node" that offers a singular API endpoint for receiving messages to send into the QNode system.

## Usage

Run `npm -i & npm start` in a downloaded copy of this repository

## To-Do

Nothing at the moment! :)

## Support

For code support, contact developers@solusproject.org

For questions or concerns about the Solus Project, contact support@solusproject.org

## Contributing

We are always open to contributions! If you feel like you can add something, feel free to make a pull request and we'll have our team of caffeinated developers review it! We love seeing our community come together to make the Solus Project what it is.

## Authors and Acknowledgements

Head Developer: William Kenzie

## License

All of our projects use the MIT License, which means you can remix or redistribute what we make however you see fit, as long as you acknowledge that you used our code.

Content like images are always licensed under the CC-BY-SHA license unless stated otherwise.

