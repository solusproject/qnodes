//faulty waz here to help

const WebSocket = require('ws');
const uuid = require('uuid');
const crypto = require('crypto');
const express = require('express')
const https = require('https')
const app = express();


const wss = new WebSocket.Server({ port: 3131 }); // 3131 is the DEFAULT port for QNodes
require('child_process').exec('rasa run --enable-api') // Make sure the RASA server is online

let clients = [];
let secret = crypto.randomBytes(8).toString('hex');

console.log('[!] QNODE SERVER SECRET: ' + secret);

wss.on('connection', (ws) => {

    ws.on('message', (_predata) => {
        let data = JSON.parse(_predata);

        if (data.type == "init") {
            let client = {
                id: uuid.v1()
            }
            clients.push(client);
            console.log(`[!] New client: ${client.id}`);
            ws.send(JSON.stringify({
                "type": "init",
                "data": client
            }));
        }

        if (data.type == "eval") {
            if (clients.includes(data.client.id) == false) {
                console.log("[!] EVAL FAILED.");
            }
            if (data.data.secret == secret) {
                console.log("[!] EVAL [" + data.data.js + "] from " + data.client.id)
                let out = eval(data.data.js);
                console.log(out);
            }
        }

    })

});

app.get('/api', (req, res) => {
    message = req.body.JSON.message
    const options = {
        hostname: 'localhost',
        port: 5050,
        path: '/model/parse',
        method: 'GET'
    }

    https.request(options, result => {
        result.on('data', d => {
            res.send(d.JSON.intent.name)
        })
    })
});

app.listen(8080)